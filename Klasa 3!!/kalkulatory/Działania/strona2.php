<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Potęgowanie</title>
    <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
<div class="baner"> 
        <a href="index.html"><img src="baner.jpg"></a> 
    </div> 
    <br> 
    <div class="lewa"> 
        Menu: 
        <br> 
        - <a href="strona1.html">proste działania</a> 
        <br> 
        - <a href="strona2.html">potęgowanie</a> 
    </div> 
 
    <div class="prawy"> 
<h1>POTĘGOWANIE</h1> 
    <label for="liczba1"><i>Podaj podstawę potęgi:</i></label> 
    <input type="text" class="liczba1" name="liczba1"> 
    <br> 
    <label for="liczba2"><i>Podaj dodatni, całkowity wykładnik potęgi:</i></label> 
    <input type="text" class="liczba2" name="liczba2"> 
    <br> 
    <button onclick="pot()">POTĘGOWANIE</button> 
    <br><br> 
    <div class="kom"></div> 
 
    <script type="text/javascript"> 
        function pot(){ 
            var liczba1 = document.querySelector(".liczba1").value; 
            var liczba2 = document.querySelector(".liczba2").value; 
            if (liczba1 == "" && liczba2 == ""){ 
                document.querySelector(".kom").innerHTML = "Wpisz podstawę i wykładnik potęgi."; 
                return; 
            } 
            else if (liczba1 == ""){ 
                document.querySelector(".kom").innerHTML = "Proszę uzupełnić podstawę potęgi."; 
                return; 
            } 
            else if (liczba2 == ""){ 
                document.querySelector(".kom").innerHTML = "Proszę uzupełnić wykładnik potęgi."; 
                return; 
            } 
            else if (parseFloat(liczba2) < 0){ 
                document.querySelector(".kom").innerHTML = "Wykładnik potęgi musi być dodatni"; 
                return; 
            } 
            var wynik = Math.pow((liczba1), parseFloat(liczba2)); 
            var komu = "Wynik działania wynosi: " + liczba1 + "^" + liczba2 + "=" + wynik; 
            document.querySelector(".kom").innerHTML = komu; 
        } 
    </script> 
    </div>
</body>
</html>