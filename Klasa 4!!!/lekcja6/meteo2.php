1. Wykonać zapytanie wyświetlające w tabeli  `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie`
2. Wykonać zapytanie wyświetlające wszystkie miasta
3. Wykonać zapytanie wyświetlające opady dla miasta o id=2
4. Wykonać zapytanie wyświetlające temperatura_noc z dni od 2020-09-14 do 2020-09-30
5. Wykonać zapytanie wyświetlające opady dla miasta o id=1
6. Wykonać zapytanie wyświetlające cisnienie między 1000 a 1018 hpa
7. Wykonać zapytanie wyświetlające średnią temperature_dzien
8. Wykonać zapytanie wyświetlające najniższą temperature_noc
9. Wykonać zapytanie wyświetlające najwyższą tempaerature_dzien
10. Wykonać zapytanie wyświetlające srednie opady 
11. Wykonać zapytanie wyświetlające ilość prognoz pogody
12. Wykonać zapytanie sortujące temperatury od najmniejszej do największej



1.SELECT `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie` FROM `pogoda` ORDER BY `data_prognozy` DESC", 1, 1, "<b><u>", "</u></b>"
2.SELECT nazwa FROM  `miasta`
3.SELECT opady FROM `pogoda` WHERE miasta_id="2"
4.SELECT temperatura_noc FROM `pogoda` WHERE `data_prognozy` > "2020-09-14" AND `data_prognozy` < "2020-09-30"
5.SELECT `opady`FROM `pogoda` WHERE `miasta_id`="1"
6.SELECT cisnienie FROM `pogoda` WHERE cisnienie >"999" AND cisnienie < "1019"
7.SELECT avg(temperatura_dzien) FROM `pogoda`
8.SELECT min(temperatura_noc) FROM `pogoda`
9.SELECT max(temperatura_dzien) FROM `pogoda`
10.SELECT avg(opady) FROM `pogoda`
11.SELECT COUNT(data_prognozy) FROM pogoda
12.SELECT temoeratura_dzien FROM pogoda ORDER BY temperatura_dzien ASC