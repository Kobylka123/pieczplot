<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style4.css">
    <?php
    // $polacz = mysqli_connect("localhost", "root", "", "prognoza") 
    ?>
    <title>Prognoza pogody Poznań</title>
    <?php
    class Database {
       public $host;
       public $user;
       public $password;
       public $db_name;

       public $sql;
       
       public function __construct() {
           $this->host = "localhost";
           $this->user = "root";
           $this->password = "";
           $this->db_name = "prognoza";
       }

       public function polaczenie() {
         return mysqli_connect($this->host, $this->user, $this->password, $this->db_name);
       }

       public function zamknij() {
           return mysqli_close($this->polaczenie());
       }

       public function executeSql($query, $display, $table, $before, $after) {
        $this->sql = $query;

        $result = $this->polaczenie()->query($this->sql);

        if ($display == 1) {
            foreach(($result->fetch_all(MYSQLI_NUM)) as $row) {
                if($table == 1) {
                    echo "<tr>";
                    foreach($row as $r) {
                        echo "<td>";
                        echo $before.$r.$after;
                        echo "</td>";
                    }
                    echo "</tr>";
                } else {
                    foreach($row as $r) {
                    echo $before.$r.$after;
                    echo "<br/>";
                    }
                } 
            }
        } else {
            return $result;
        }
       }

    }

    $polacz = (new Database())->polaczenie();
    $zamknij = (new Database())->zamknij();
    
    //exit();
    ?>
</head>
<body>
    <section class="baner_lewy">
    <p>maj 2019r.</p>
    </section>
    <section class="baner_srodkowy">
    <h2>Prognoza dla Poznania</h2>
    </section>
    <section class="baner_prawy">
    <img src="./logo.png" alt="prognoza">
    </section>
    <section class="blok_lewy">
    <a href="./kwerendy.txt">Kwerendy</a>
    </section>
    <section class="blok_prawy">
    <img src="./obraz.jpg" alt="Polska, Poznań">
    </section>
    <section class="blok_glowny">
    <table>
        <tr>
        
            <th>Lp.</th>
            <th>MIASTO ID</th>
            <th>DATA</th>
            <th>NOC - TEMPERATURA</th>
            <th>DZIEŃ - TEMPERATURA</th>
            <th>OPADY [mm/h]</th>
            <th>CIŚNIENIE [hPa]</th>
        
        </tr>
        <tr>
            <td>
                <?php
    $sql1 = (new Database())->executeSql("SELECT `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie` FROM `pogoda` ORDER BY `data_prognozy` DESC", 1, 1, "<b><u>", "</u></b>");





                // exit();
                // $sql= "SELECT `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie` FROM `pogoda` WHERE `miasta_id` = '2' ORDER BY `data_prognozy` DESC";
                // $result=mysqli_query($polacz, $sql);
                // $id=0;
                // while($row=mysqli_fetch_array($result)) {
                //     $id++;
                //     echo "<tr>";
                //     echo "<td>";
                //     echo $id;
                //     echo "</td>";
                //     echo "<td>";
                //     echo $row['data_prognozy'];
                //     echo "</td>";
                //     echo "<td>";
                //     echo $row['temperatura_noc'];
                //     echo "</td>";
                //     echo "<td>";
                //     echo $row['temperatura_dzien'];
                //     echo "</td>";
                //     echo "<td>";
                //     echo $row['opady'];
                //     echo "</td>";
                //     echo "<td>";
                //     echo $row['cisnienie'];
                //     echo "</td>";
                //     echo "</tr>";
                // }
                ?>
            </td>
        </tr>
    </table>

    <?php
    $sql1 = (new Database())->executeSql("SELECT `id`, `miasta_id`, `data_prognozy`, `temperatura_noc`, `temperatura_dzien`, `opady`, `cisnienie` FROM `pogoda` ORDER BY `data_prognozy` DESC", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT `nazwa` FROM  `miasta` " , 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT `opady` FROM `pogoda` WHERE `miasta_id`='2' ", 1, 0, "<b><u>", "</u></b>");

    $sql1 = (new Database())->executeSql("SELECT `temperatura_noc` FROM `pogoda` WHERE `data_prognozy` > '2020-09-14' AND `data_prognozy` < '2020-09-30' ", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT `opady`FROM `pogoda` WHERE `miasta_id`='1' " , 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT `cisnienie` FROM `pogoda` WHERE `cisnienie` >'999' AND `cisnienie` < '1019' ", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT avg(`temperatura_dzien`) FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT min(`temperatura_noc`) FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT max(`temperatura_dzien`) FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT avg(`opady`) FROM `pogoda`", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT COUNT(`data_prognozy`) FROM `pogoda` ", 1, 0, "<b><u>", "</u></b>");
    
    $sql1 = (new Database())->executeSql("SELECT `temperatura_dzien` FROM `pogoda` ORDER BY `temperatura_dzien` ASC ", 1, 0, "<b><u>", "</u></b>");
    







    ?>
    </section>
    <section class="stopka">
    <p>Stronę wykonał: Team Kaziu And Domin Sp.Zo.O.</p>
    </section>
    <?php
        mysqli_close($polacz);
    ?>
</body>
</html>